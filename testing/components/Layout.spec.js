const { shallowMount } = require('@vue/test-utils');
const Layout = require('@/components/Layout.vue');



describe('Layout.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(Layout);
  });

  it('renders a navbar', () => {
    expect(wrapper.find('.navbar').exists()).toBe(true);
  });


  it('has a brand logo', () => {
    const logo = wrapper.find('.navbar-brand img');
    expect(logo.exists()).toBe(true);
    expect(logo.attributes('src')).toBe('../assets/WSU_secondary_logo2.png');
  });

  it('contains link to Home', () => {
    const homeLink = wrapper.find('.navbar-nav a[href="http://localhost:10200/"]');
    expect(homeLink.text()).toBe('Home');
  });

  it('contains link to Checkout', () => {
    const checkoutLink = wrapper.find('.navbar-nav a[href="http://localhost:10202/"]');
    expect(checkoutLink.text()).toBe('Checkout');
  });

  it('contains link to Check Inventory', () => {
    const inventoryLink = wrapper.find('.navbar-nav a[href="http://localhost:10203/"]');
    expect(inventoryLink.text()).toBe('Check Inventory');
  });

  it('toggles the navigation on small screens', async () => {
    expect(wrapper.find('.collapse.navbar-collapse').isVisible()).toBe(false);
    await wrapper.find('.navbar-toggler').trigger('click');
    expect(wrapper.find('.collapse.navbar-collapse').isVisible()).toBe(true);
  });
});
