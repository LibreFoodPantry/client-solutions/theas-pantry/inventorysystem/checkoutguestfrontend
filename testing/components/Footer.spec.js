const { shallowMount } = require('@vue/test-utils');
const Footer = require('@/components/Footer.vue');

describe('Footer.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(Footer);
  });

  it('renders the footer', () => {
    expect(wrapper.find('.sticky-footer').exists()).toBe(true);
  });

  it('has the correct link', () => {
    const link = wrapper.find('#content a');
    expect(link.exists()).toBe(true);
    expect(link.attributes('href')).toBe('https://www.worcester.edu/Theas-Pantry/');
  });

  it('displays the correct text for the link', () => {
    const link = wrapper.find('#content a');
    expect(link.text()).toBe('Website');
  });

  it('displays the copyright notice correctly', () => {
    const text = wrapper.find('#content p');
    expect(text.text()).toBe("Thea's Pantry 2024");
  });

  it('has correct styles for footer', () => {
    const footer = wrapper.find('.sticky-footer');
    expect(footer.attributes('style')).toContain('background-color: #003087');
  });
});
