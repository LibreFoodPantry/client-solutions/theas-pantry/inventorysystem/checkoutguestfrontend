const { shallowMount } = require('@vue/test-utils');
const Main = require('@/components/Main.vue');

describe('Main.vue', () => {
    it('Empty Form Submission', async () => {
      const wrapper = shallowMount(Main);
      await wrapper.find('form').trigger('submit');
      expect(console.log).not.toHaveBeenCalled();
    });
  
    it('Valid Form Submission', async () => {
      const wrapper = shallowMount(Main);
      await wrapper.setData({ wsuId: '12345', pounds: 5 });
      await wrapper.find('form').trigger('submit');
      expect(console.log).toHaveBeenCalledWith('Submitting WSU ID: 12345 with Pounds: 5');
    });
  
    it('Invalid WSU ID', async () => {
      const wrapper = shallowMount(Main);
      await wrapper.setData({ wsuId: '   ', pounds: 5 });
      await wrapper.find('form').trigger('submit');
      expect(console.log).not.toHaveBeenCalled();
    });
  
    it('Invalid Pounds', async () => {
      const wrapper = shallowMount(Main);
      await wrapper.setData({ wsuId: '12345', pounds: -5 });
      await wrapper.find('form').trigger('submit');
      expect(console.log).not.toHaveBeenCalled();
    });
  
    it('Clear Input', async () => {
      const wrapper = shallowMount(Main);
      await wrapper.setData({ wsuId: '12345', pounds: 5 });
      await wrapper.find('button[type="button"]').trigger('click');
      expect(wrapper.vm.wsuId).toBe('');
      expect(wrapper.vm.pounds).toBe(0);
      expect(wrapper.vm.submitEnabled).toBe(false);
    });
  
    it('WSU ID Only', async () => {
      const wrapper = shallowMount(Main);
      await wrapper.setData({ wsuId: '12345', pounds: 0 });
      await wrapper.find('form').trigger('submit');
      expect(console.log).not.toHaveBeenCalled();
    });
  
    it('Pounds Only', async () => {
      const wrapper = shallowMount(Main);
      await wrapper.setData({ wsuId: '', pounds: 5 });
      await wrapper.find('form').trigger('submit');
      expect(console.log).not.toHaveBeenCalled();
    });
  
    it('Multiple Submission Attempts', async () => {
      const wrapper = shallowMount(Main);
      await wrapper.setData({ wsuId: '12345', pounds: 5 });
      await wrapper.find('form').trigger('submit');
      expect(console.log).toHaveBeenCalledWith('Submitting WSU ID: 12345 with Pounds: 5');
  
      await wrapper.setData({ wsuId: '54321', pounds: 10 });
      await wrapper.find('form').trigger('submit');
      expect(console.log).toHaveBeenCalledWith('Submitting WSU ID: 54321 with Pounds: 10');
    });
  });
