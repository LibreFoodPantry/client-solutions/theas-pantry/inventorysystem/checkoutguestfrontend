import { createApp } from 'vue'

import App from './App.vue'
import piniaPluginPersistedState from 'pinia-plugin-persistedstate'

import './css/bootstrap.min.css'

let url;
if (import.meta.env.VITE_BASE_URL != undefined) {
  url = import.meta.env.VITE_BASE_URL;
} else {
  url = "https://localhost:10350";
}
export const BACKEND_BASE_URL = url;

const app = createApp(App)


app.mount('#app')
