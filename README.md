# Orders

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Start backend

1. `bin/backend-down.sh`
2. `bin/backend-up.sh`
3. `bin/backend-data.sh` *(Optional to add starting data)*

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


# Based off of this tutorial:
https://github.com/mdobydullah/run-vue.js-app-with-docker
