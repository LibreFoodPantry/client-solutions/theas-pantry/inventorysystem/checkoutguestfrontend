#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

cd "$SCRIPT_DIR/.." || exit
docker compose -f "${SCRIPT_DIR}"/../testing/backend-server/docker-compose.yaml up
