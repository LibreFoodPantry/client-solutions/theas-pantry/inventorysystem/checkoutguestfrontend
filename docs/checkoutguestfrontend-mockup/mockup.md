# CheckOutGuestFrontend Mockup

![CheckOutGuestFrontend Mockup Image](mockup.png "Mockup")
Part 1: Coding UI to place components on the screen where we want them (“layout”) - Weight 1
Part 2: Handling text/input fields properly (input validation/boundary checks?) - Weight 2
Part 3: Button functionality - interaction/sending to other screens, changing inventory - Weight 2
